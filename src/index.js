require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const path = require('path')
const cors = require('cors')
const _PORT = process.env.PORT || 3001;

const MONGODB_URL = `${process.env.MONGO_URL}`;

const MONGO_CONFIG = {
  useNewUrlParser: true,
  useUnifiedTopology: true
}

const app = express();
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
app.use(morgan('dev'));
app.use("/files", express.static(path.resolve(__dirname, '..', 'tmp', 'uploads')))
app.use(cors())

mongoose.connect(
  MONGODB_URL,
  MONGO_CONFIG,
  (err, res) => {
    if (err) {
      console.log(err)
      throw new Error("connection to mongo failed", err)
    }
    console.log("connected with mongodb database")
  })

app.use('/', require('./routes.js'))

app.listen(_PORT, () => {
  console.log(`server start at ${process.env.APP_URL}`)
})