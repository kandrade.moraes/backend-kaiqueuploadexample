const routes = require('express').Router();
const multer = require('multer');
const multerConfig = require('./config/multer');

const Post = require('./models/Post')

routes.get('/posts', async (req, res)=>{
  const POSTS = await Post.find()
  res.status(200).json(POSTS)
})

routes.post('/posts', multer(multerConfig).single('file'), async (req, res)=>{
  const {originalname : name, size, key, location : url = ''} = req.file;

  const post = await Post.create({
    name,
    size,
    key,
    url,
  })
  res.status(200).json(post)
})

routes.delete('/posts/:id', multer(multerConfig).single('file'), async (req, res)=>{
  const post = await Post.findById(req.params.id);

  await post.remove()

  res.status(204).json()
});

module.exports = routes;